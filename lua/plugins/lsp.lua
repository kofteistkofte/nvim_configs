return {
    {
        'VonHeikemen/lsp-zero.nvim',
        dependencies = {
            {
                'williamboman/mason.nvim',
                build = function ()
                    vim.cmd("MasonUpdate")
                end,
            },
            'neovim/nvim-lspconfig',
            'williamboman/mason-lspconfig.nvim',
            'SmiteshP/nvim-navic',
            'folke/neodev.nvim',
            {
                'hrsh7th/nvim-cmp',
                dependencies = {
                    'hrsh7th/cmp-buffer',
                    'hrsh7th/cmp-path',
                    'hrsh7th/cmp-nvim-lsp',
                    'hrsh7th/cmp-nvim-lua',

                    -- Snippets
                    'L3MON4D3/LuaSnip',
                    'rafamadriz/friendly-snippets',
                    'saadparwaiz1/cmp_luasnip',
                },
            },
        },
        opts = {
            servers = {
                pylsp = {
                    settings = {
                        pylsp = {
                            plugins = {
                                pycodestyle = {
                                    enabled = true,
                                    maxLineLength = 120
                                },
                            }
                        }
                    }
                },
                lua_ls = {
                    settings = {
                        Lua = {
                            completion = {
                                callSnippet = "Replace"
                            }
                        }
                    }
                }
            },
        },
        config = function (_, opts)
            local utils = require('core.utils')

            local lsp = require('lsp-zero')
            local navic = require('nvim-navic')
            local cmp = require('cmp')

            require("neodev").setup{}

            local cmp_select = {behavior = cmp.SelectBehavior.Select}
            local cmp_mappings = lsp.defaults.cmp_mappings({
                ['<C-p>'] = cmp.mapping.select_prev_item(cmp_select),
                ['<C-n>'] = cmp.mapping.select_next_item(cmp_select),
                ['<C-y>'] = cmp.mapping.confirm({select = true}),
                ['<CR>'] = cmp.mapping.confirm({select = true}),
                ["<C-Space>"] = cmp.mapping.complete(),
                ['<Tab>'] = nil,
                ['<S-Tab>'] = nil,
            })

            lsp.preset('recommended')
            lsp.ensure_installed({'rust_analyzer', 'pylsp'})


            lsp.nvim_workspace()
            lsp.setup_nvim_cmp({mapping = cmp_mappings})

            for server, config in pairs(opts.servers) do
                lsp.configure(server, config)
            end

            lsp.on_attach(function(client, bufnr)
                navic.attach(client, bufnr)

                local kopts = {buffer = bufnr, remap = false}

                if client.name == "eslint" then
                    vim.cmd.LspStop('eslint')
                    return
                end

                utils.keymap{key="gd", command=vim.lsp.buf.definition, opts=kopts, desc='[Lsp] goto definition'}
                utils.keymap{key="gr", command=vim.lsp.buf.references, opts=kopts, desc='[Lsp] list references'}
                utils.keymap{key="df", command=vim.diagnostic.open_float, opts=kopts, desc='[Lsp] list references'}
                utils.keymap{key="<leader>D", command=vim.lsp.buf.type_definition, opts=kopts, desc='[Lsp] type definition'}
                utils.keymap{key="K", command=vim.lsp.buf.hover, opts=kopts, desc='[Lsp] toggle info'}
                utils.keymap{key="<leader>vd", command=vim.lsp.buf.workspace_symbol, opts=kopts, desc='[Lsp] workspace symbols'}
                utils.keymap{key="[d", command=vim.diagnostic.goto_next, opts=kopts, desc='[Lsp] goto next diagnostic'}
                utils.keymap{key="]d", command=vim.diagnostic.goto_prev, opts=kopts, desc='[Lsp] goto previous diagnostic'}
                utils.keymap{key="<leader>ca", command=vim.lsp.buf.code_action, opts=kopts, desc='[Lsp] code action'}
                utils.keymap{key="<leader>rn", command=vim.lsp.buf.rename, opts=kopts, desc='[Lsp] rename'}
                utils.keymap{mode='i', key='<C-h>', command=vim.lsp.buf.signature_help, opts=kopts, desc='[Lsp] signature help'}
            end)

            navic.setup({
                highlight = true,
            })

            lsp.setup()
            require'lspconfig'.qml_lsp.setup{
                filetypes = { 'qmljs', 'qml' },
                on_attach = function(client, bufnr)
                    client.server_capabilities.semanticTokensProvider = nil
                end,
            }
        end
    },
}
